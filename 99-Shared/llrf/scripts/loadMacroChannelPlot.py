from org.csstudio.display.builder.runtime.script import PVUtil

# run only if there is some valid channel
if len(PVUtil.getString(pvs[0]).split(" - ")) > 1 :
    channel = PVUtil.getString(pvs[0]).split(" - ")[1]

    # Adding the macros on the widget that will consume this script
    widget.getPropertyValue("macros").add("CHANNEL", channel)

    # if is a down-sampled or PIERR channel get DAQ format from PV
    if len(channel.split("Dwn")) > 1 or ("RFCErr" in channel) :
        prefix = PVUtil.getString(pvs[1])
        if len(channel.split("Dwn")) > 1:
            pv_daq = PVUtil.createPV(prefix + channel + "-Fmt-RB", 5000)
        else:
            pv_daq = PVUtil.createPV(prefix + channel + "Fmt-RB", 5000) #Naming different for PIERR
        daqfmt = PVUtil.getString(pv_daq)
        widget.getPropertyValue("macros").add("CMP0", daqfmt.split("/")[0])
        if len(daqfmt.split("/")) > 1:
            widget.getPropertyValue("macros").add("CMP1", daqfmt.split("/")[1])
        else:
            widget.getPropertyValue("macros").add("CMP1", "NONE")
    else: #otherwise the default format is IQ
        widget.getPropertyValue("macros").add("CMP0", "I")
        widget.getPropertyValue("macros").add("CMP1", "Q")

    if len(channel.split("IntCh")) > 1:
        widget.getPropertyValue("macros").add("AXSEP", "")
    else:
        widget.getPropertyValue("macros").add("AXSEP", "-")

    widget.setPropertyValue("file", "")
    widget.setPropertyValue("file", "plot_ch.bob")
